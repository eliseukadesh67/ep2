package transportes;

public class Caminhao extends Veiculo {
	
	public Caminhao(String codigo){
		setCodigo(codigo);
		setCombustivel("Diesel");
		setRendimento(8.0f);
		setCargamax(30000f);
		setVelocidade(60);
		setReducao(0.0002f);
		setPreco(getCombustivel());
	}

}
