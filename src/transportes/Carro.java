package transportes;

public class Carro extends Veiculo{
	
	public Carro(String codigo, String combustivel){
		setCodigo(codigo);
		setCombustivel(combustivel);
		setCargamax(360f);
		setVelocidade(100f);
		setPreco(getCombustivel());
		
		if(getCombustivel()=="Álcool") {
			setRendimento(12f);
			setReducao(0.0231f);
		}
		else {
			setRendimento(14f);
			setReducao(0.025f);
		}
		
	}
	
}
