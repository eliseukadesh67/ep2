package transportes;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class Frota {
	
	private ArrayList<Veiculo> veiculos_frota = new ArrayList<>();
	private ArrayList<Veiculo> veiculos_disp = new ArrayList<>();
	private ArrayList<Veiculo> veiculos_on_service = new ArrayList<>();
	private ArrayList<Veiculo> candidates = new ArrayList<>();
	private String diretorio_frota_disp = "/home/eliseu/Área de Trabalho/OO_2019/Java/ep2/src/arq/frota.dat";
	
	
	public Frota() {
		carregar_frota_geral();
		carregar_frota();
	}
	
	public int get_array_size() {
		
		return veiculos_disp.size();
	}
	public Veiculo get_candidate(int index) {
		return candidates.get(index);
	}
	public void clear_candidates() {
		candidates.clear();
	}
	public void ad_veiculo(Veiculo v) {
		
		veiculos_frota.add(v);
		veiculos_disp.add(v);
	}
	public boolean retira_veiculo(String code) {
		int i=0;
		int pos=0;
		for(Veiculo it : veiculos_disp) {
			String b = it.getCodigo();
			if(code.contentEquals(b)) {
				pos=i;
			}
			i++;
		}
		
		System.out.println("TAMANHO:"+veiculos_disp.size());
		
		if(pos>0) {
			System.out.println(pos);
			veiculos_frota.remove(pos);
			veiculos_disp.remove(pos);
			grava_frota_geral();
			System.out.println("TAMANHO:"+veiculos_disp.size());
			return true;
		}
		else {
			return false;
		}
	}
	public void clear_list() {
		veiculos_disp.clear();
	}
	
	// Criar diretório e arquivos de dados
	public void CriaArquivos() {
	// Verificar se a pasta de arquivos ja existe, senão, criá-la.
	java.io.File direct = new java.io.File("arq");
	if(direct.exists()==false) {
		direct.mkdir();
	}
	// Verificar se o arquivo de veículos disponiveis já existe.
	java.io.File f = new java.io.File("data/disponiveis.dat");
	
		if(f.exists()==false) {
			try {
				f.createNewFile();
			}
			catch(IOException ex) {
			}
		}
	}
	public void carregar_frota_geral() {
		
		   try
		   {
		     //Carrega o arquivo
		     FileInputStream arquivoLeitura = new FileInputStream(diretorio_frota_disp);
		           // Classe responsavel por recuperar os objetos do arquivo
		     ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
		     veiculos_frota = (ArrayList<Veiculo>)objLeitura.readObject();
		     objLeitura.close();
		     arquivoLeitura.close();
		   }
		   catch(Exception e) {
		     e.printStackTrace();
		   }
		  
		  
	}
	public void grava_frota_geral() {
		
		System.out.println("NO GRAVA:"+veiculos_disp.size());
	
		//Adicionando veiculo a frota
		
		try
	    {
	      //Gera o arquivo para armazenar o objeto
	      FileOutputStream arquivoGrav = new FileOutputStream(diretorio_frota_disp);
	      //Classe responsavel por inserir os objetos
	      ObjectOutputStream objGravar = new ObjectOutputStream(arquivoGrav);
	      //Grava o objeto no arquivo
	      objGravar.flush();
	      arquivoGrav.flush();
	      objGravar.writeObject(veiculos_frota);
	      objGravar.flush();
	      objGravar.close();
	      arquivoGrav.flush();
	      arquivoGrav.close();
	    }
	 
	    catch(Exception e) {
	      e.printStackTrace();
	    }
		    
	}
	
	
	public void carregar_frota() {
		
		   try
		   {
		     //Carrega o arquivo
		     FileInputStream arquivoLeitura = new FileInputStream(diretorio_frota_disp);
		           // Classe responsavel por recuperar os objetos do arquivo
		     ObjectInputStream objLeitura = new ObjectInputStream(arquivoLeitura);
		     veiculos_disp = (ArrayList<Veiculo>)objLeitura.readObject();
		     objLeitura.close();
		     arquivoLeitura.close();
		   }
		   catch(Exception e) {
		     e.printStackTrace();
		   }
		  
		  
		   
	}
	public boolean disponivel() {
		if(veiculos_disp.isEmpty()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	
	public void set_candidates(float rend) {
		
		int i=0;
		int pos=0;
		
		for(Veiculo it : veiculos_disp) {
			if(rend == it.getRendimento()) {
				pos=i;
				break;
			}
			i++;
			
		}
			candidates.add(veiculos_disp.get(pos));
		
	}
	public boolean verifica_existe(float rend) {
		
		int cont=0;
		
		for(Veiculo it : veiculos_disp) {
			if(rend == it.getRendimento()) {
				cont++;
				break;
			}
		}
		
		if(cont>0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	public ArrayList<Veiculo> get_all_candidates(){
		return candidates;
	}
	
	public void show_disp() {
		carregar_frota_geral();
		
		mostra_carro();
		mostra_moto();
		mostra_van();
		mostra_cam();
	}
	
	public void mostra_carro() {
		
		System.out.println("Carros");
		System.out.println("Codigo do veiculo:\t\tCombustivel");
		for(Veiculo it : veiculos_frota) {
			if(it.getRendimento()==14f || it.getRendimento()==12f) {
				
			}
		}
	}
	public void mostra_moto() {
		
		System.out.println("Motos");
		System.out.println("Codigo do veiculo:\t\tCombustivel");
		for(Veiculo it : veiculos_frota) {
			if(it.getRendimento()==50f || it.getRendimento()==43f) {
			}
		}
	}
	public void mostra_van() {

		System.out.println("Vans");
		System.out.println("Codigo do veiculo:\t\tCombustivel");
		for(Veiculo it : veiculos_frota) {
			if(it.getRendimento()==10f) {
				System.out.println(it.getCodigo() +"\t\t"+it.getCombustivel());
			}
		}
		
	}
	public void mostra_cam() {

		System.out.println("Caminhões");
		System.out.println("Codigo do veiculo:\t\tCombustivel");
		for(Veiculo it : veiculos_frota) {
			if(it.getRendimento()==8f) {
				System.out.println(it.getCodigo() +"\t\t"+it.getCombustivel());
			}
		}
		
	}
	public boolean send_to_serivice(Veiculo vei) {
		int i=0;
		int pos=0;
		
		if(veiculos_disp.isEmpty()) {
			return false;
		}
		else {
			
			for(Veiculo it : veiculos_frota) {
			
				if(it.getCodigo().equals(vei.getCodigo())) {
					pos=i;
					veiculos_on_service.add(it);
					System.out.println("foi pro servico");
				}
			
				i++;
			}
			System.out.println("POs:"+pos);
			System.out.println(veiculos_disp.size());
			veiculos_disp.remove(pos);
		
			return true;
		}
		
		
	}
	public void get_on_service() {
		System.out.println("Tipo:\t\tCodigo:");
		System.out.println("QUANT DE ENTREGAS: "+veiculos_on_service.size());
		for(Veiculo it : veiculos_on_service) {
			if(it.getVelocidade()==100) {
				System.out.print("Carro");
			}
			else if(it.getVelocidade()==110) {
				System.out.print("Moto");
			}
			else if (it.getVelocidade()==80) {
				System.out.print("Van");
			}
			else {
				System.out.print("Caminhão");
			}
			System.out.println("\t\t"+it.getCodigo());
		}
	}
	public Veiculo get_last_on_service() {
		return veiculos_on_service.get(veiculos_on_service.size()-1);
	}
	public int get_candidates_size() {
		return candidates.size();
	}
}