package transportes;

public class Moto extends Veiculo{
	
	public Moto(String codigo, String combustivel){
		setCodigo(codigo);
		setCombustivel(combustivel);
		setCargamax(50f);
		setVelocidade(110f);
		setPreco(getCombustivel());
		
		if(getCombustivel()=="Álcool") {
			setRendimento(43f);
			setReducao(0.4f);
		}
		else {
			setRendimento(50f);
			setReducao(0.3f);
		}
		
	}
	
}


