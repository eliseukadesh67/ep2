package transportes;


import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;


public class Sistema {
	
	private float peso;
	private float distancia;
	private float temp_max;
	private float margem_lucro;
	private Frota frota = new Frota();
	private ArrayList<String> menor_cust = new ArrayList<>();
	private ArrayList<String> maior_benef = new ArrayList<>();
	private ArrayList<String> menor_temp = new ArrayList<>();
	
	public ArrayList<String> getMenor_cust() {
		return menor_cust;
	}

	public void setMenor_cust(String menor_cust) {
		this.menor_cust.add(menor_cust);
		
	}
	public ArrayList<String> getMaior_benef() {
		return maior_benef;
	}

	public void setMaior_benef(String maior_benef) {
		this.maior_benef.add(maior_benef);
	}

	public ArrayList<String> getMenor_temp() {
		return menor_temp;
	}

	public void setMenor_temp(String menor_temp) {
		this.menor_temp.add(menor_temp);
	}
	
	public float getMargem_lucro() {
		return margem_lucro;
	}

	public void setMargem_lucro(float margem_lucro) {
		this.margem_lucro = margem_lucro;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public float getDistancia() {
		return distancia;
	}

	public void setDistancia(float distancia) {
		this.distancia = distancia;
	}

	public float getTemp_max() {
		return temp_max;
	}

	public void setTemp_max(float temp_max) {
		this.temp_max = temp_max;
	}
	public boolean entrega(float dist, float temp, float pes) {
		
		
		//Setar as informações nas variaveis da classe do sistema//
		setDistancia(dist);
		setPeso(pes);
		setTemp_max(temp);
		//Chamar a função para efetuar os devidos calculos//
		
			if(!frota.disponivel()) {
				return false;
			}
			
			frota.clear_candidates();
			
			//System.out.println("MENOR CUSTO DE OPERAÇÃO: ");
			calcula_menor_custo(dist, pes, temp);
		//
			//System.out.println("MENOR TEMPO DE ENTREGA: ");
			calcula_menor_tempo(dist,temp);
			
		//
			//System.out.println("MELHOR CUSTO BENEFICIO: ");
			calcula_cust_beneficio(dist);
		//
			//System.out.println("Informe qual das tres opções deseja escolher: ");
			//int escolha = ler.nextInt();
			//ystem.out.println("Tamanho candidates"+frota.get_candidates_size());
			//frota.send_to_serivice(frota.get_candidate(escolha-1));
			//frota.clear_candidates();
			//frota.get_on_service();
			//ler.close();
			System.out.println("MENOR CUSTO DE OPERAÇÃO: ");
			System.out.println("Custo total: "+ get_candidates().get(0).getCusto());
			System.out.println("Tempo de Entrega: "+get_candidates().get(0).getTempo());
			System.out.println("Rendimento: "+get_candidates().get(0).getRendimento());
			System.out.println("Codigo do veiculo: "+get_candidates().get(0).getCodigo());
			System.out.println("MENOR TEMPO DE ENTREGA: ");
			System.out.println("Custo total: "+ get_candidates().get(1).getCusto());
			System.out.println("Tempo de Entrega: "+get_candidates().get(1).getTempo());
			System.out.println("Rendimento: "+get_candidates().get(1).getRendimento());
			System.out.println("Codigo do veiculo: "+get_candidates().get(1).getCodigo());
			System.out.println("MELHOR CUSTO BENEFICIO: ");
			System.out.println("Custo total: "+ get_candidates().get(2).getCusto());
			System.out.println("Tempo de Entrega: "+get_candidates().get(2).getTempo());
			System.out.println("Rendimento: "+get_candidates().get(2).getRendimento());
			System.out.println("Codigo do veiculo: "+get_candidates().get(2).getCodigo());
			
			System.out.println("CODIGO DO VEICULO EM SERVIÇO: "+get_candidates().get(0).getCodigo());
			
			String f1 = Float.toString(get_candidates().get(0).getRendimento());
			String f2 = Float.toString(get_candidates().get(0).getTempo());
			String f3 = Float.toString(get_candidates().get(0).getCusto());
			String f4 = Float.toString(get_candidates().get(1).getRendimento());
			String f5 = Float.toString(get_candidates().get(1).getTempo());
			String f6 = Float.toString(get_candidates().get(1).getCusto());
			String f7 = Float.toString(get_candidates().get(2).getRendimento());
			String f8 = Float.toString(get_candidates().get(2).getTempo());
			String f9 = Float.toString(get_candidates().get(2).getCusto());
			
					
			setMenor_cust(f1);
			setMenor_cust(f2);
			setMenor_cust(f3);
			
			setMenor_temp(f4);
			setMenor_temp(f5);
			setMenor_temp(f6);
			
			setMaior_benef(f7);
			setMaior_benef(f8);
			setMaior_benef(f9);
			
			return true;
			
	}

	public void calcula_menor_custo(float distancia, float peso, float tempo) {
		
		float menor_custo=0f;
		float marg = getMargem_lucro();
		int aux=0;
		
		frota.clear_candidates();
		//
		ArrayList<Float> valores = new ArrayList<>();
	
		valores.add(get_custos().get(0));
		valores.add(get_custos().get(1));
		valores.add(get_custos().get(2));
		valores.add(get_custos().get(3));
		valores.add(get_custos().get(4));
		valores.add(get_custos().get(5));
		
		menor_custo = get_custos().get(2);
	
		for(float it : valores) {
			if(it>0f) {
				aux++;
				
				if(aux==1) {
					menor_custo = it;
				}
				else if(it<menor_custo) {
					menor_custo = it; 
				} 
					
			}
		}
		
		if(get_custos().get(0).equals(menor_custo )) {
			frota.set_candidates(12f);
			frota.get_candidate(0).setTempo(distancia/frota.get_candidate(0).getVelocidade());
		}
		else if(menor_custo == get_custos().get(1)) {
			System.out.println("ENTROU");
			frota.set_candidates(14f);
			frota.get_candidate(0).setTempo(distancia/frota.get_candidate(0).getVelocidade());		}
		else if (menor_custo == get_custos().get(2)) {
			System.out.println("ENTROU");
			frota.set_candidates(43f);
			frota.get_candidate(0).setTempo(distancia/frota.get_candidate(0).getVelocidade());;		}
		else if(menor_custo == get_custos().get(3)) {
			System.out.println("ENTROU");
			frota.set_candidates(50f);
			frota.get_candidate(0).setTempo((distancia/frota.get_candidate(0).getVelocidade()));		}
		else if(menor_custo == get_custos().get(4)) {
			System.out.println("ENTROU");
			frota.set_candidates(10f);
			frota.get_candidate(0).setTempo((distancia/frota.get_candidate(0).getVelocidade()));		}
		else {
			System.out.println("ENTROU");
			frota.set_candidates(8f);
			frota.get_candidate(0).setTempo(distancia/frota.get_candidate(0).getVelocidade());
		}
		frota.get_candidate(0).setCusto((menor_custo*(1+marg)));
		
	}
	
	public float custo_operação(Veiculo v) {
	
		float total;
		float temp_gasto;
		float rend_total;
		
		temp_gasto = distancia/v.getVelocidade();
		rend_total = v.getRendimento() - (v.getReducao()*peso);
		
			if(peso>v.getCargamax() || temp_gasto>temp_max) {
				total = 0f;
			}
			else {
				total = (distancia/rend_total) * v.getPreco();
			}
			
		return total;
	}
	public void ad_frota(String tipo, String code, String comb) {
		
		frota.carregar_frota_geral();
		
		if(tipo=="Carro") {
			
			Veiculo vei = new Carro(code, comb);
			
			
			frota.ad_veiculo(vei);
		}
		else if(tipo=="Moto") {

			System.out.println("Adicionou");
			Veiculo vei = new Moto(code, comb);
			
			
			frota.ad_veiculo(vei);
			
		}
		else if(tipo=="Caminhão") {
			
			Veiculo vei = new Caminhao(code);
			
			
			frota.ad_veiculo(vei);
			
			
		}
		else{
			
			Veiculo vei = new Van(code);
			
			
			frota.ad_veiculo(vei);
		}
		
		frota.grava_frota_geral();
		
		System.out.println("Veiculo Salvo com sucesso!");
	}
	public boolean retira_frota(String code) {
		
		if(frota.retira_veiculo(code)) {
			return true;
		}
		else {
			return false;
		}
	}
	public void acessar_frota() {
		
		frota.carregar_frota_geral();
		
		frota.show_disp();
			
	}
	public void calcula_menor_tempo(float dist, float tempo) {
		float tempo_car;
		float tempo_moto;
		float tempo_van;
		float tempo_caminhao;
		float menor_tempo;
		
		if(peso>360f) {
			tempo_car = 0f;
		}
		else {
			tempo_car = calcula_tempo(dist,100f);
		}
		if(peso>50f) {
			tempo_moto = 0f;
		}
		else {
			tempo_moto = calcula_tempo(dist, 110f);
		}
		if(peso>3500f) {
			tempo_van = 0f;
		}
		else {
			tempo_van = calcula_tempo(dist, 80f);
		}
		if(peso>30000) {
			tempo_caminhao = 0f;
		}
		else {
			tempo_caminhao = calcula_tempo(dist, 60f);
		}
		
		
		ArrayList<Float> temps = new ArrayList<Float>();
		
		if(frota.verifica_existe(14f)) {
			temps.add(tempo_car);
		}
		else {
			temps.add(0f);
		}
		if(frota.verifica_existe(50f)) {
			temps.add(tempo_moto);
		}
		else {
			temps.add(0f);
		}
		if(frota.verifica_existe(10f)) {
			temps.add(tempo_van);
		}
		else {
			temps.add(0f);
		}
		if(frota.verifica_existe(8f)) {
			temps.add(tempo_caminhao);
		}
		else {
			temps.add(0f);
		}
		
		menor_tempo = tempo_moto;
		
		int aux=0;
				
			for(float it : temps) {
					
				System.out.println("TEMPO DE CADA UM: "+it);
				if(it>0f) {
						aux++;
						
						if(aux==1) {
							menor_tempo = it;
						}
						else if(it<menor_tempo && it<temp_max) {
							menor_tempo = it; 
						} 
							
					}
				}
			
			
			System.out.println("MENOR TEMPO: "+menor_tempo);
			
		if(menor_tempo==tempo_car) {
			System.out.println("Carro!");
			frota.set_candidates(14f);
			frota.get_candidate(1).setTempo((menor_tempo));
			frota.get_candidate(1).setCusto((get_custos().get(1)*(1+(getMargem_lucro()))));
			
		}
		else if(menor_tempo==tempo_moto) {
			System.out.println("Moto!");
			frota.set_candidates(50f);
			frota.get_candidate(1).setTempo((menor_tempo));
			frota.get_candidate(1).setCusto((get_custos().get(3)*(1+(getMargem_lucro()))));
			
		}
		else if(menor_tempo==tempo_van) {
			System.out.println("Van!");
			frota.set_candidates(10f);
			frota.get_candidate(1).setTempo((menor_tempo));
			frota.get_candidate(1).setCusto((get_custos().get(4)*(1+(getMargem_lucro()))));
		}
		else if(menor_tempo==tempo_caminhao) {
			System.out.println("Cam!");
			frota.set_candidates(8f);
			frota.get_candidate(1).setTempo((menor_tempo));
			frota.get_candidate(1).setCusto((get_custos().get(5)*(1+(getMargem_lucro()))));
			
		}
	}
	public float calcula_tempo(float dist, float velocidade) {
		return dist/velocidade;
	}
	public void calcula_cust_beneficio(float dist) {
		float benef_car1;
		float benef_car2;
		float benef_moto1;
		float benef_moto2;
		float benef_van;
		float benef_caminhao;
		float melhor_cust;
		
		benef_car1 = get_custos().get(0)/calcula_tempo(dist, 100f);
		benef_car2 = get_custos().get(1)/calcula_tempo(dist, 100f);
		benef_moto1 = get_custos().get(2)/calcula_tempo(dist, 110f);
		benef_moto2 = get_custos().get(3)/calcula_tempo(dist, 110f);
		benef_van = get_custos().get(4)/calcula_tempo(dist, 80f);
		benef_caminhao = get_custos().get(5)/calcula_tempo(dist, 60f);
		
		ArrayList<Float> benefs = new ArrayList<Float>();
		
		benefs.add(benef_car1);
		benefs.add(benef_car2);
		benefs.add(benef_moto1);
		benefs.add(benef_moto2);
		benefs.add(benef_van);
		benefs.add(benef_caminhao);
		
		melhor_cust = benef_car2;
		
		for(float it : benefs) {
			if(it>melhor_cust && it>0f) {
				melhor_cust = it;
			}
			
		}
		if(melhor_cust==benef_car1) {
			System.out.println("CARRO");
			frota.set_candidates(12f);
			frota.get_candidate(2).setTempo(calcula_tempo(dist, 100f));
			frota.get_candidate(2).setCusto((get_custos().get(0)*(1+(getMargem_lucro()))));
			//System.out.println("FOI");
		}
		else if(melhor_cust==benef_car2) {
			System.out.println("CARRO");
			frota.set_candidates(14f);
			frota.get_candidate(2).setTempo(calcula_tempo(dist, 100f));
			frota.get_candidate(2).setCusto((get_custos().get(1)*(1+(getMargem_lucro()))));
			//System.out.println("FOI");
		}
		else if(melhor_cust==benef_moto1) {
			System.out.println("MOTO");
			frota.set_candidates(43f);
			frota.get_candidate(2).setTempo(calcula_tempo(dist, 100f));
			frota.get_candidate(2).setCusto((get_custos().get(2)*(1+(getMargem_lucro()))));
			//System.out.println("FOI");
		}
		else if(melhor_cust==benef_moto2) {
			System.out.println("MOTO");
			frota.set_candidates(50f);
			frota.get_candidate(2).setTempo(calcula_tempo(dist, 100f));
			frota.get_candidate(2).setTempo((get_custos().get(3)*(1+(getMargem_lucro()))));
			//System.out.println("FOI");
			
		}
		else if(melhor_cust==benef_van) {
			System.out.println("VAN");
			frota.set_candidates(10f);
			frota.get_candidate(2).setTempo(calcula_tempo(dist, 100f));
			frota.get_candidate(2).setCusto((get_custos().get(4)*(1+(getMargem_lucro()))));
			//System.out.println("FOI");
		}
		else if(melhor_cust==benef_caminhao) {
			System.out.println("CAM");
			frota.set_candidates(8f);
			frota.get_candidate(2).setTempo(calcula_tempo(dist, 100f));
			frota.get_candidate(2).setCusto((get_custos().get(5)*(1+(getMargem_lucro()))));
			//System.out.println("FOI");
		}
	}
	public ArrayList<Float> get_custos() {
		Veiculo car1 = new Carro(null, "Álcool");
		Veiculo car2 = new Carro(null, "Gasolina");
		Veiculo moto1 = new Moto(null, "Álcool");
		Veiculo moto2 = new Moto(null, "Gasolina");
		Veiculo van = new Van(null);
		Veiculo cam = new Caminhao(null);
		ArrayList<Float> custs = new ArrayList<Float>();
		
		//Efetuar os calculos//
		if(frota.verifica_existe(12f)) {
			custs.add(custo_operação(car1));
		}
		else {
			custs.add(0f);
		}
		if(frota.verifica_existe(14f)) {
			custs.add(custo_operação(car2));
		}
		else {
			custs.add(0f);
		}
		if(frota.verifica_existe(43f)) {
			custs.add(custo_operação(moto1));
		}
		else {
			custs.add(0f);
		}
		if(frota.verifica_existe(50f)) {
			custs.add(custo_operação(moto2));
		}
		else {
			custs.add(0f);
		}
		if(frota.verifica_existe(10f)) {
			custs.add(custo_operação(van));
		}
		else {
			custs.add(0f);
		}
		if(frota.verifica_existe(8f)) {
			custs.add(custo_operação(cam));
		}
		else {
			custs.add(0f);
		}
		
		return custs;
	
}
	public ArrayList<Veiculo> get_candidates() {
		
		return frota.get_all_candidates();
	}
	public Veiculo get_select() {
		return frota.get_last_on_service();
	}
	public void finaliza_servico(int index) {
		frota.send_to_serivice(get_candidates().get(index));
	}
}