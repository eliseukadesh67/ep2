package transportes;

public class Van extends Veiculo{
	
	public Van(String codigo){
		setCodigo(codigo);
		setCombustivel("Diesel");
		setRendimento(10f);
		setCargamax(3500f);
		setVelocidade(80f);
		setReducao(0.001f);
		setPreco(getCombustivel());
	}

}
