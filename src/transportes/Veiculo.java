package transportes;

import java.io.Serializable;


public abstract class Veiculo implements Serializable{
	private String codigo;
	private String combustivel;
	private float rendimento;
	private float cargamax;
	private float velocidade;
	private float reducao;
	private float preco;
	private float custo;
	private float tempo;
	
	public float getCusto() {
		return custo;
	}
	public void setCusto(float custo) {
		this.custo = custo;
	}
	public float getTempo() {
		return tempo;
	}
	public void setTempo(float tempo) {
		this.tempo = tempo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCombustivel() {
		return combustivel;
	}
	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}
	public float getRendimento() {
		return rendimento;
	}
	public void setRendimento(float rendimento) {
		this.rendimento = rendimento;
	}
	public float getCargamax() {
		return cargamax;
	}
	public void setCargamax(float cargamax) {
		this.cargamax = cargamax;
	}
	public float getVelocidade() {
		return velocidade;
	}
	public void setVelocidade(float velocidade) {
		this.velocidade = velocidade;
	}
	public float getReducao() {
		return reducao;
	}
	public void setReducao(float reducao) {
		this.reducao = reducao;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(String combustivel) {
		
		if(combustivel=="Diesel") {
			this.preco = 3.869f;
		}
		if(combustivel=="Álcool") {
			this.preco =  3.499f;
		}
		else {
			this.preco =  4.449f;
		}
	}

}
