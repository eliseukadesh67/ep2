package visual;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import transportes.Sistema;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Adiciona_veiculos extends JFrame {

	private Sistema sistema = new Sistema();
	private JPanel contentPane;
	private JTextField textFieldCodigo;

	/**
	 * Launch the application.
	 */
	public void cria() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Adiciona_veiculos frame = new Adiciona_veiculos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Adiciona_veiculos() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setBounds(30, 55, 66, 15);
		contentPane.add(lblTipo);
		
		JLabel lblNewLabel = new JLabel("Codigo");
		lblNewLabel.setBounds(30, 171, 66, 19);
		contentPane.add(lblNewLabel);
		
		textFieldCodigo = new JTextField();
		textFieldCodigo.setBounds(30, 202, 124, 19);
		contentPane.add(textFieldCodigo);
		textFieldCodigo.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Combustivel");
		lblNewLabel_1.setBounds(30, 113, 97, 15);
		contentPane.add(lblNewLabel_1);
		
		JComboBox comboBoxCombustivel = new JComboBox();
		comboBoxCombustivel.setModel(new DefaultComboBoxModel(new String[] {"Gasolina", "Álcool"}));
		comboBoxCombustivel.setBounds(30, 140, 111, 19);
		contentPane.add(comboBoxCombustivel);
		
		JComboBox combotipo = new JComboBox();
		combotipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(combotipo.getSelectedItem()=="Carro" || combotipo.getSelectedItem()=="Moto") {
					comboBoxCombustivel.setEnabled(true);
				}
				else {
					comboBoxCombustivel.setEnabled(false);
				}
			}
		});
		
		combotipo.setModel(new DefaultComboBoxModel(new String[] {"Moto", "Carro", "Caminhão", "Van"}));
		combotipo.setBounds(30, 82, 111, 19);
		contentPane.add(combotipo);
		
		
		JButton btnNewButton = new JButton("Adicionar novo veiculo á frota");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String str1 = (String)combotipo.getSelectedItem();
				String str2 = (String)comboBoxCombustivel.getSelectedItem();
				String str3 = textFieldCodigo.getText();
				System.out.println("Tipo:"+str1);
				sistema.ad_frota(str1, str3,str2);
				JOptionPane.showMessageDialog(contentPane, "Veículo cadastrado!");
			}
		});
		btnNewButton.setBounds(22, 233, 236, 25);
		contentPane.add(btnNewButton);
		
		JLabel lblCadastroDe = new JLabel("CADASTRO DE VEÍCULOS ");
		lblCadastroDe.setFont(new Font("URW Gothic L", Font.BOLD, 20));
		lblCadastroDe.setBounds(87, 12, 286, 31);
		contentPane.add(lblCadastroDe);
	}
}
