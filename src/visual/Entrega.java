package visual;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import transportes.Sistema;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;

public class Entrega extends JFrame {

	private JPanel JPanelentrega_principal;
	private JTextField textField_distancia;
	private JTextField textField_peso;
	private JTextField textField_tempomax;
	private JLabel lblInformaesDaEntrega;
	private JLabel lblKm;
	private JLabel lblKg;
	private JLabel lblHoras;

	/**
	 * Launch the application.
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
			try {
					Entrega frame = new Entrega(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Entrega(Sistema sis) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		JPanelentrega_principal = new JPanel();
		JPanelentrega_principal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(JPanelentrega_principal);
		JPanelentrega_principal.setLayout(null);
		
		textField_distancia = new JTextField();
		textField_distancia.setBounds(22, 76, 124, 19);
		JPanelentrega_principal.add(textField_distancia);
		textField_distancia.setColumns(10);
		
		textField_peso = new JTextField();
		textField_peso.setBounds(22, 136, 124, 19);
		JPanelentrega_principal.add(textField_peso);
		textField_peso.setColumns(10);
		
		textField_tempomax = new JTextField();
		textField_tempomax.setBounds(22, 191, 124, 19);
		JPanelentrega_principal.add(textField_tempomax);
		textField_tempomax.setColumns(10);
		
		JLabel JLabeldistancia = new JLabel("Distancia");
		JLabeldistancia.setBounds(22, 49, 66, 15);
		JPanelentrega_principal.add(JLabeldistancia);
		
		JLabel JLabelpeso = new JLabel("Peso de carga");
		JLabelpeso.setBounds(22, 103, 135, 27);
		JPanelentrega_principal.add(JLabelpeso);
		
		JLabel JLabeltempmax = new JLabel("Tempo máximo de entrega");
		JLabeltempmax.setBounds(22, 164, 183, 15);
		JPanelentrega_principal.add(JLabeltempmax);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				float f = Float.parseFloat(textField_distancia.getText());
				float f1 = Float.parseFloat(textField_peso.getText());
				float f2 = Float.parseFloat(textField_tempomax.getText());
				
				if(!sis.entrega(f, f2, f1)) {
					JOptionPane.showMessageDialog(JPanelentrega_principal, "Erro! Não há veículos disponíveis!");
				}
				else {
					ResultadoEntrega resultado = new ResultadoEntrega(sis);
					resultado.setVisible(true);
				}
			
			}
		});
		btnCalcular.setBounds(22, 233, 114, 25);
		JPanelentrega_principal.add(btnCalcular);
		
		lblInformaesDaEntrega = new JLabel("Informações da entrega:");
		lblInformaesDaEntrega.setBounds(12, 12, 181, 15);
		JPanelentrega_principal.add(lblInformaesDaEntrega);
		
		lblKm = new JLabel("(Km)");
		lblKm.setBounds(152, 76, 66, 15);
		JPanelentrega_principal.add(lblKm);
		
		lblKg = new JLabel("(Kg)");
		lblKg.setBounds(152, 136, 66, 15);
		JPanelentrega_principal.add(lblKg);
		
		lblHoras = new JLabel("(Horas)");
		lblHoras.setBounds(152, 191, 66, 15);
		JPanelentrega_principal.add(lblHoras);
	}
}
