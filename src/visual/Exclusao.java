package visual;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import transportes.Sistema;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Exclusao extends JFrame {

	private JPanel JPanelexclusao;
	private JTextField textFieldCodigoeclusao;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Exclusao frame = new Exclusao(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Exclusao(Sistema sis) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		JPanelexclusao = new JPanel();
		JPanelexclusao.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(JPanelexclusao);
		JPanelexclusao.setLayout(null);
		
		JLabel JLabelcodigo_exclusao = new JLabel("Insira o código do veículo que deseja retirar.");
		JLabelcodigo_exclusao.setForeground(Color.BLACK);
		JLabelcodigo_exclusao.setFont(new Font("DejaVu Sans Condensed", Font.BOLD, 14));
		JLabelcodigo_exclusao.setBounds(51, 79, 328, 30);
		JPanelexclusao.add(JLabelcodigo_exclusao);
		
		textFieldCodigoeclusao = new JTextField();
		textFieldCodigoeclusao.setBounds(81, 121, 269, 19);
		JPanelexclusao.add(textFieldCodigoeclusao);
		textFieldCodigoeclusao.setColumns(10);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!sis.retira_frota(textFieldCodigoeclusao.getText())) {
					JOptionPane.showMessageDialog(JPanelexclusao, "Erro! O veículo é inexistente ou está em uma entrega!");
				}
				else {
					JOptionPane.showMessageDialog(JPanelexclusao, "Veículo removido!");
				}
			}
		});
		btnExcluir.setBounds(152, 152, 114, 25);
		JPanelexclusao.add(btnExcluir);
	}
}
