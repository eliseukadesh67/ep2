package visual;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import transportes.Sistema;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class Menu extends JFrame {

	private JPanel contentPane;
	private Sistema sis = new Sistema();

	/**
	 * Launch the application.
	 */
	public void Cria() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Menu() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Realizar Entrega");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Entrega entrega = new Entrega(sis);
				entrega.setVisible(true);
			}
		});
		btnNewButton.setBounds(129, 71, 189, 25);
		contentPane.add(btnNewButton);
		
		JButton btncadastro = new JButton("Cadastrar um veículo");
		btncadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Adiciona_veiculos ad = new Adiciona_veiculos();
				ad.setVisible(true);
			}
		});
		btncadastro.setBounds(129, 108, 189, 25);
		contentPane.add(btncadastro);
		
		JLabel lblBemVindo = new JLabel("Bem vindo!");
		lblBemVindo.setFont(new Font("URW Gothic L", Font.BOLD, 20));
		lblBemVindo.setBounds(156, 22, 138, 25);
		contentPane.add(lblBemVindo);
		
		JButton btnNewButton_1 = new JButton("Excluir um veículo");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Exclusao ex = new Exclusao(sis);
				ex.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(129, 145, 189, 25);
		contentPane.add(btnNewButton_1);
	}
}
