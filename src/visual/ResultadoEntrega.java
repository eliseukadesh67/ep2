package visual;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import transportes.Sistema;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.text.DecimalFormat;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ResultadoEntrega extends JFrame {

	private JPanel JPanelresultadoentrega;

	/**
	 * Launch the application.
	 */
	public void start() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ResultadoEntrega frame = new ResultadoEntrega(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ResultadoEntrega(Sistema sis) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 567, 404);
		JPanelresultadoentrega = new JPanel();
		JPanelresultadoentrega.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(JPanelresultadoentrega);
		JPanelresultadoentrega.setLayout(null);
		
		JLabel lblResutadosEncntrados = new JLabel("Resutados encontrados:");
		lblResutadosEncntrados.setForeground(Color.BLACK);
		lblResutadosEncntrados.setFont(new Font("URW Gothic L", Font.BOLD, 17));
		lblResutadosEncntrados.setBounds(192, 0, 211, 36);
		JPanelresultadoentrega.add(lblResutadosEncntrados);
		
		JLabel lblMenorCustoDe = new JLabel("Menor custo de operação");
		lblMenorCustoDe.setFont(new Font("Dialog", Font.BOLD, 14));
		lblMenorCustoDe.setBounds(12, 48, 220, 15);
		JPanelresultadoentrega.add(lblMenorCustoDe);
		
		JLabel lblTipoDoVeculo = new JLabel("Tipo do veículo:");
		lblTipoDoVeculo.setBounds(12, 73, 116, 15);
		JPanelresultadoentrega.add(lblTipoDoVeculo);
		
		JLabel lbltipo1 = new JLabel("New label");
		lbltipo1.setBounds(157, 73, 126, 15);
		JPanelresultadoentrega.add(lbltipo1);
			
		if("12.0".equals(sis.getMenor_cust().get(0))) {
				lbltipo1.setText("Carro(álcool)");
			}
			else if("14.0".equals(sis.getMenor_cust().get(0))) {
				lbltipo1.setText("Carro(gasolina)");
			}
			else if("43.0".equals(sis.getMenor_cust().get(0))) {
				lbltipo1.setText("Moto(álcool)");
			}
			else if("50.0".equals(sis.getMenor_cust().get(0))) {
				lbltipo1.setText("Moto(gasolina)");
			}
			else if("10.0".equals(sis.getMenor_cust().get(0))) {
				lbltipo1.setText("Van");
			}
			else {
				lbltipo1.setText("Caminhão");
			}
		
		DecimalFormat duascasas = new DecimalFormat("0.00");
		DecimalFormat umacasa = new DecimalFormat("0.0");
			
		JLabel lblNewLabel_1 = new JLabel("Tempo de entrega:");
		lblNewLabel_1.setBounds(12, 102, 138, 15);
		JPanelresultadoentrega.add(lblNewLabel_1);
		
		JLabel lbltempo1 = new JLabel("New label");
		lbltempo1.setBounds(157, 100, 37, 15);
		JPanelresultadoentrega.add(lbltempo1);
		float f1 = Float.valueOf(sis.getMenor_cust().get(1).trim()).floatValue();
		lbltempo1.setText(umacasa.format(f1));
		
		JLabel lblNewLabel_3 = new JLabel("Custo total");
		lblNewLabel_3.setBounds(12, 129, 116, 15);
		JPanelresultadoentrega.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("R$");
		lblNewLabel_4.setBounds(157, 127, 66, 15);
		JPanelresultadoentrega.add(lblNewLabel_4);
		
		JLabel lblMenorTempoDe = new JLabel("Menor tempo de entrega");
		lblMenorTempoDe.setFont(new Font("Dialog", Font.BOLD, 14));
		lblMenorTempoDe.setBounds(12, 156, 211, 15);
		JPanelresultadoentrega.add(lblMenorTempoDe);
		
		JLabel lblNewLabel_5 = new JLabel("Tipo do veículo");
		lblNewLabel_5.setBounds(12, 178, 116, 15);
		JPanelresultadoentrega.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Tempo de entrega");
		lblNewLabel_6.setBounds(12, 205, 138, 15);
		JPanelresultadoentrega.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Custo total");
		lblNewLabel_7.setBounds(12, 232, 94, 15);
		JPanelresultadoentrega.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("Maior custo benefício");
		lblNewLabel_8.setFont(new Font("Dialog", Font.BOLD, 14));
		lblNewLabel_8.setBounds(12, 259, 211, 15);
		JPanelresultadoentrega.add(lblNewLabel_8);
		
		JButton btnenviarmenorcusto = new JButton("Enviar");
		btnenviarmenorcusto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sis.finaliza_servico(0);
				JOptionPane.showMessageDialog(JPanelresultadoentrega, "Veículo do código: "+sis.get_candidates().get(0).getCodigo()+" enviado com sucesso!");
			setVisible(false);
			}
		});
		btnenviarmenorcusto.setBounds(445, 68, 94, 25);
		JPanelresultadoentrega.add(btnenviarmenorcusto);
		
		JButton btnEnviarmenortempo = new JButton("Enviar");
		btnEnviarmenortempo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sis.finaliza_servico(1);
				JOptionPane.showMessageDialog(JPanelresultadoentrega, "Veículo do código: "+sis.get_candidates().get(1).getCodigo()+" enviado com sucesso!");
				setVisible(false);
			}
		});
		btnEnviarmenortempo.setBounds(445, 200, 94, 25);
		JPanelresultadoentrega.add(btnEnviarmenortempo);
		
		JLabel lbltipodoveículo2 = new JLabel("New label");
		lbltipodoveículo2.setBounds(157, 178, 126, 15);
		JPanelresultadoentrega.add(lbltipodoveículo2);
		
		if("12.0".equals(sis.getMenor_temp().get(0))) {
			lbltipodoveículo2.setText("Carro(álcool)");
		}
		else if("14.0".equals(sis.getMenor_temp().get(0))) {
			lbltipodoveículo2.setText("Carro(gasolina)");
		}
		else if("43.0".equals(sis.getMenor_temp().get(0))) {
			lbltipodoveículo2.setText("Moto(álcool)");
		}
		else if("50.0".equals(sis.getMenor_temp().get(0))) {
			lbltipodoveículo2.setText("Moto(gasolina)");
		}
		else if("10.0".equals(sis.getMenor_temp().get(0))) {
			lbltipodoveículo2.setText("Van");
		}
		else {
			lbltipodoveículo2.setText("Caminhão");
		}
		
		JLabel lbltempo2 = new JLabel("New label");
		lbltempo2.setBounds(157, 205, 30, 15);
		JPanelresultadoentrega.add(lbltempo2);
		float f3 = Float.valueOf(sis.getMenor_temp().get(1).trim()).floatValue();
		lbltempo2.setText(umacasa.format(f3));
		
		JLabel lblcusto2 = new JLabel("New label");
		lblcusto2.setBounds(188, 232, 66, 15);
		JPanelresultadoentrega.add(lblcusto2);
		float f4 = Float.valueOf(sis.getMenor_temp().get(2).trim()).floatValue();
		lblcusto2.setText(duascasas.format(f4));
		
		JLabel lblcusto1 = new JLabel("New label");
		lblcusto1.setBounds(188, 127, 66, 15);
		JPanelresultadoentrega.add(lblcusto1);
		lblcusto1.setText(sis.getMenor_cust().get(2));
		float f2 = Float.valueOf(sis.getMenor_cust().get(2).trim()).floatValue();
		lblcusto1.setText(duascasas.format(f2));
		
		JLabel lblR = new JLabel("R$");
		lblR.setBounds(157, 232, 30, 15);
		JPanelresultadoentrega.add(lblR);
		
		JLabel lblHoras = new JLabel("Horas");
		lblHoras.setBounds(188, 205, 66, 15);
		JPanelresultadoentrega.add(lblHoras);
		
		JLabel lblHoras_1 = new JLabel("Horas");
		lblHoras_1.setBounds(201, 100, 66, 15);
		JPanelresultadoentrega.add(lblHoras_1);
		
		JLabel lblNewLabel = new JLabel("Tipo do veículo");
		lblNewLabel.setBounds(12, 286, 116, 15);
		JPanelresultadoentrega.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Tempo de entrega");
		lblNewLabel_2.setBounds(12, 313, 138, 15);
		JPanelresultadoentrega.add(lblNewLabel_2);
		
		JLabel lblNewLabel_9 = new JLabel("Custo total");
		lblNewLabel_9.setBounds(12, 340, 94, 15);
		JPanelresultadoentrega.add(lblNewLabel_9);
		
		JLabel lbltipodeveiculo3 = new JLabel("New label");
		lbltipodeveiculo3.setBounds(157, 286, 126, 15);
		JPanelresultadoentrega.add(lbltipodeveiculo3);
		
		if("12.0".equals(sis.getMaior_benef().get(0))) {
			lbltipodeveiculo3.setText("Carro(álcool)");
		}
		else if("14.0".equals(sis.getMaior_benef().get(0))) {
			lbltipodeveiculo3.setText("Carro(gasolina)");
		}
		else if("43.0".equals(sis.getMaior_benef().get(0))) {
			lbltipodeveiculo3.setText("Moto(álcool)");
		}
		else if("50.0".equals(sis.getMaior_benef().get(0))) {
			lbltipodeveiculo3.setText("Moto(gasolina)");
		}
		else if("10.0".equals(sis.getMaior_benef().get(0))) {
			lbltipodeveiculo3.setText("Van");
		}
		else {
			lbltipodeveiculo3.setText("Caminhão");
		}
		
		JLabel lbltemp03 = new JLabel("New label");
		lbltemp03.setBounds(157, 313, 37, 15);
		JPanelresultadoentrega.add(lbltemp03);
		float f5 = Float.valueOf(sis.getMaior_benef().get(1).trim()).floatValue();
		lbltemp03.setText(umacasa.format(f5));
		
		JLabel lblcusto3 = new JLabel("New label");
		lblcusto3.setBounds(188, 340, 66, 15);
		JPanelresultadoentrega.add(lblcusto3);
		float f6 = Float.valueOf(sis.getMaior_benef().get(2).trim()).floatValue();
		lblcusto3.setText(duascasas.format(f6));
		
		JLabel lblHoras_2 = new JLabel("Horas");
		lblHoras_2.setBounds(188, 313, 66, 15);
		JPanelresultadoentrega.add(lblHoras_2);
		
		JLabel lblR_1 = new JLabel("R$");
		lblR_1.setBounds(157, 340, 17, 15);
		JPanelresultadoentrega.add(lblR_1);
		
		JButton btnEnviarmaiorbeneficio = new JButton("Enviar");
		btnEnviarmaiorbeneficio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				sis.finaliza_servico(2);
				JOptionPane.showMessageDialog(JPanelresultadoentrega, "Veículo do código: "+sis.get_candidates().get(2).getCodigo()+" enviado com sucesso!");
				setVisible(false);
			}
		});
		btnEnviarmaiorbeneficio.setBounds(445, 308, 94, 25);
		JPanelresultadoentrega.add(btnEnviarmaiorbeneficio);
	}
}
